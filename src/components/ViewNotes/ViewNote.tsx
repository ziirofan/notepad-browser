import { useLiveQuery } from 'dexie-react-hooks';
import { useState } from 'react';
import { 
    Grid, 
    GridItem,
    Box,
    Button,
    VStack,
    StackDivider,
    Flex,
    Container,
} from '@chakra-ui/react';
import { db } from '../../db/db';
import { Note } from '../../db/interfaces/note'
import Style from './ViewNotes.module.scss';
import Showdown from 'showdown';
import parse from 'html-react-parser';

export function ViewNote(props: any){
    const {carnetId} = props

    const converter = new Showdown.Converter()
    
    const [note, setNote] = useState<Note>();
    const notes = useLiveQuery(
        () => db.notes.where("carnet").equals(carnetId).toArray(),
        [carnetId]
    )
    return(
        <div>
            <Grid
            templateAreas={`"side main"`}
            gridTemplateRows={'10fr'}
            gridTemplateColumns={'2fr 8fr'}
            h='800px'
            gap='1'
            color='blackAlpha.700'
            fontWeight='bold'>

            <GridItem pl='1' bg='gray.400' area={'side'}>
            <VStack
                divider={<StackDivider borderColor='gray.200' />}
                spacing={1}
                align='stretch'
                >
                            {
                                notes?.map((noteEl:any) => 
                                    <div key={noteEl.id}>
                                        
                                        <Button
                                            size='md'
                                            height='60px'
                                            width='100%'
                                            border='1px'
                                            borderColor='grey.500'
                                            onClick={() => setNote(noteEl)}
                                        >
                                                {noteEl.title}
                                            </Button>
                                        
                                    </div>)
                            }
                        </VStack>
                
            </GridItem>
            <GridItem pl='2' bg='gray.100' area={'main'}>
                <Flex direction="column">
                    <Box
                        bg="gray.400"
                        w="100%"
                        borderWidth='1px' 
                        borderRadius='lg'
                        >
                            {note ? note.title: null}
                        </Box>

                    <Box
                    w="100%"
                    h="90vh"
                    bg="gray.300"
                    borderWidth='1px' 
                    borderRadius='lg'
                    textAlign='left'
                    >
                        <Container w="100%" marginInlineStart="2" className={Style.notesDisplayMd}>
                            
                                {note ? parse(converter.makeHtml(note.text)): null}
                            
                        </Container>
                    </Box>
                </Flex>
                
            </GridItem>
        </Grid>
        </div>
    )
}
