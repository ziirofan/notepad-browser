import { useLiveQuery } from 'dexie-react-hooks';
import { useState } from 'react';
import { db } from '../../db/db';
import { NoteByCarnet } from '../Notes/GetNote';

export function CarnetList(){
    const carnets = useLiveQuery(
        () => db.carnets.toArray()
    )

    return <ul>
        {carnets?.map(carnet => <li key={carnet.id}>
            <CarnetNoteList carnetId={carnet.id} />
        </li>)}
    </ul>;
}

export function CarnetNoteList(props: any){
    const [notes, setNotes] = useState(false);
    
    const { carnetId } = props;
    const carnets = useLiveQuery(
        () => db.carnets.where("id").equals(carnetId).toArray()
    )


    return <ul>
    {carnets?.map(carnet => <li key={carnet.id}>
        <h2 onClick={()=> setNotes(!notes)}>{carnet.title}</h2>
        <div>{notes ? <NoteByCarnet carnetId={carnet.id}/> : null}</div>
    </li>)}
</ul>;

}