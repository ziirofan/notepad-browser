import { useLiveQuery } from 'dexie-react-hooks';
import { db } from '../../db/db';

/*export function getNote(props: any){
    const { noteId } = props;
    const note = useLiveQuery(
        () => db.notes.where("id").equals(noteId)
    )

    return <div>
        <h2>{note.title}</h2>
            <p>{note.text}</p>
            <p>{new Date(note.date).toLocaleDateString()}</p>
    </div>
}*/

export function NoteList(){
    const notes = useLiveQuery(
        () => db.notes.toArray()
    )

    return <ul>
        {notes?.map(note => <li key={note.id}>
            <h2>{note.title}</h2>
            <p>{note.text}</p>
            <p>{new Date(note.date).toLocaleDateString()}</p>
        </li>)}
    </ul>;
}

export function NoteByCarnet(props: any){
    const { notes } = props;
    console.log(notes)
    return <ul>
        {notes?.map((note:any) => <li key={note.id}>
            <h2>{note.title}</h2>
        </li>)}
    </ul>;
}