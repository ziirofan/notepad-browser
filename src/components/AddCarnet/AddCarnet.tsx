import { 
    useDisclosure,
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    Button,
    MenuItem,
    Input,
    useToast
} from '@chakra-ui/react';
import { useState, useRef } from 'react';
import { db } from '../../db/db';

export function AddCarnetForm({}) {
    const [title, setTitle] = useState("");
    const [statusMessage, setStatusMessage] = useState("");
    const { isOpen, onOpen, onClose } = useDisclosure()
    const btnRef = useRef()
    const toast = useToast()

    async function addCarnet(){
        try{
            const id = await db.carnets.add({
                title,
                numberOfNotes: 0,
            });
            setStatusMessage(`Carnet ${title} successfully added`);
            setTitle("");
            onClose();
            return (
                toast({
                    title: 'Carnet Created',
                    description: statusMessage,
                    status: 'success',
                    duration: 9000,
                    isClosable: true,
                  })
            )
        }
        catch(error){
            console.error(error)
            setStatusMessage(`Failed to add ${title}: ${error}`);
            onClose();
            return (
                toast({
                    title: 'Error',
                    description: statusMessage,
                    status: 'error',
                    duration: 9000,
                    isClosable: true,
                  })
            )
        }
    }

    return (
        <>
        <MenuItem onClick={onOpen}>
          Add Carnet
        </MenuItem>
        <Drawer
          isOpen={isOpen}
          placement='right'
          onClose={onClose}
        >
          <DrawerOverlay />
          <DrawerContent>
            <DrawerCloseButton />
            <DrawerHeader>Create Carnet</DrawerHeader>
  
            <DrawerBody>
                <div>
                    <Input type="text" value={title} onChange={ev => setTitle(ev.target.value)}/>
                </div>
            </DrawerBody>
  
            <DrawerFooter>
              <Button variant='outline' mr={3} onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme='blue' onClick={addCarnet}>Save</Button>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>
      </>
    );

}