import { useLiveQuery } from 'dexie-react-hooks';
import { useState } from 'react';
import { 
    Grid, 
    GridItem,
    VStack,
    StackDivider,
    Button,
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuDivider,
    Box, 
} from '@chakra-ui/react';
import { AddNoteForm } from '../AddNote/AddNoteForm';
import { AddCarnetForm } from '../AddCarnet/AddCarnet';
import { CarnetList, CarnetNoteList } from '../Carnet/GetCarnet';
import { ViewNote } from '../ViewNotes/ViewNote';
import { NoteByCarnet } from '../Notes/GetNote';
import { db } from '../../db/db';
import logo from '../../logo.svg'


export function Home() {
    const[dispNoteForm, setDispNoteForm] = useState(false) 
    const[dispCarnetForm, setDispCarnetForm] = useState(false)
    const[carnetId, setCarnetId] = useState(0);
    const[dispNotes, setDispNotes] = useState(false);


    const carnets = useLiveQuery(
        () => db.carnets.toArray()
    )

    const notes = useLiveQuery(
        () => db.notes.where("carnet").equals(carnetId).toArray(),
        [carnetId]
    )

  
    return (
      <div>
        <Grid
            templateAreas={`"header header header"
                            "nav main main"
                            "footer footer footer"`}
            gridTemplateRows={'1fr 8fr 1px'}
            gridTemplateColumns={'1fr 2fr 7fr'}
            h='900px'
            gap='1'
            color='blackAlpha.700'
            fontWeight='bold'
        >

            <GridItem pl='2' bg='gray.600' area={'header'}>
                <Box
                display='flex'
                alignItems='left'
                >

                <Menu>
                    <MenuButton
                        px={8}
                        py={2}
                        transition='all 0.2s'
                        borderRadius='md'
                        borderWidth='1px'
                        _hover={{ bg: 'gray.400' }}
                        _expanded={{ bg: 'blue.400' }}
                        _focus={{ boxShadow: 'outline' }}
                    >
                        Menu
                    </MenuButton>
                    <MenuList>
                        <AddNoteForm/>
                        <MenuItem>Add Todo</MenuItem>
                        <AddCarnetForm/>
                        <MenuDivider />
                        <MenuItem>Export/Import</MenuItem>
                        <MenuItem>Settings</MenuItem>
                    </MenuList>
                </Menu>
                
                </Box>
                
            </GridItem>
            
            <GridItem pl='2' bg='gray.300' area={'nav'}>
            <VStack
                divider={<StackDivider borderColor='gray.200' />}
                spacing={1}
                align='stretch'
                >
                {carnets?.map(carnet => 
                <Button
                onClick={()=> {
                    console.log(carnet.id)
                    setCarnetId(carnet.id!)
                    }}
                key={carnet.id}
                size='md'
                height='30px'
                width='90%'
                border='2px'
                borderColor='grey.500'
                >
                    {carnet.title}
                </Button>  
                )}
            </VStack>
            </GridItem>
            
            <GridItem bg='gray.400' area={'main'}>
                <ViewNote carnetId={carnetId}/>
            </GridItem>

            <GridItem pl='2' bg='gray.600' area={'footer'}>
                Footer
            </GridItem>

        </Grid>    
      </div>
    )
  }