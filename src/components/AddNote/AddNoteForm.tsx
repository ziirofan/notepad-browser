import { useState, useRef } from 'react';
import { useLiveQuery } from 'dexie-react-hooks';
import { db } from '../../db/db';
import {
    useDisclosure,
    Drawer,
    DrawerBody,
    DrawerFooter,
    DrawerHeader,
    DrawerOverlay,
    DrawerContent,
    DrawerCloseButton,
    MenuItem,
    Textarea,
    Input,
    InputGroup,
    InputLeftAddon,
    Select,
    Button,
    VStack,
    useToast
 } from '@chakra-ui/react'

export function AddNoteForm({}) {
    const [title, setTitle] = useState("");
    const [text, setText] = useState("");
    const [tagString, setTagString] = useState("");
    const [carnetS, setCarnetS] = useState("1");
    const { isOpen, onOpen, onClose } = useDisclosure()
    const btnRef = useRef()

    const toast = useToast()

    const carnets = useLiveQuery(
        () => db.carnets.toArray()
    )

    async function addNote(){
        const date = Date.now()
        const tags = tagString.split(',');
        const carnet = parseInt(carnetS, 10)
        try{
            const id = await db.notes.add({
                title,
                text,
                date,
                tags,
                carnet
            });
            const carnetInDb = await db.carnets.get(carnet)
            const stat = await db.carnets.update(carnet, {numberOfNotes: carnetInDb?.numberOfNotes! + 1})
            setTitle("");
            setText("");
            setTagString("");
            onClose();
            return toast({
                title: 'Note Created',
                description: `Note ${title} successfully added`,
                status: 'success',
                variant: 'top-accent',
                duration: 7000,
                isClosable: true,
            })
        }
        catch(error){
            console.error(error)
            onClose();
            return toast({
                title: 'Error',
                description: `Failed to add ${title}: ${error}`,
                status: 'error',
                variant: 'top-accent',
                duration: 7000,
                isClosable: true,
            })
        }
    }

    return (
        <>
        <MenuItem onClick={onOpen}>
          Add Note
        </MenuItem>
        <Drawer
          isOpen={isOpen}
          placement='right'
          onClose={onClose}
          size='xl'
        >
          <DrawerOverlay />
          <DrawerContent>
            <DrawerCloseButton />
            <DrawerHeader>Create Note</DrawerHeader>
  
            <DrawerBody>
            <InputGroup>
                <InputLeftAddon children='Titre' />
                <Input type="text" value={title} onChange={ev => setTitle(ev.target.value)} placeholder="Titre de la note"/>
            </InputGroup>
            
            <Textarea value={text} onChange={ev => setText(ev.target.value)} rows={22} cols={10} placeholder="Début de note..."/>

            <InputGroup>
                <InputLeftAddon children='Tags' />
                <Input type="text" value={tagString} onChange={ev => setTagString(ev.target.value)} placeholder="tag1,tag2"/>
            </InputGroup>
            <Select onChange={ev => setCarnetS(ev.target.value)} placeholder="Choisir Carnet">
                    {carnets?.map((option, index)=>(
                        <option key={index} value={option.id}>{option.title}</option>
                    ))}
            </Select>
            </DrawerBody>
  
            <DrawerFooter>
              <Button variant='outline' mr={3} onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme='blue' onClick={addNote}>Save</Button>
            </DrawerFooter>
          </DrawerContent>
        </Drawer>
      </>
        
    );

}