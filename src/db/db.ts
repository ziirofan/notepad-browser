import Dexie, { Table } from "dexie";
import { Note } from './interfaces/note';
import { Carnet } from './interfaces/carnet';

export class MySubClassedDexie extends Dexie{
    notes!: Table<Note>;
    carnets!: Table<Carnet>;

    constructor() {
        super('NotepadDB');
        this.version(2).stores({
            notes: '++id, title, text, date, tags, carnet',
            carnets: '++id, title, numberOfNotes'
        });
    }
}

export const db = new MySubClassedDexie(); 