export interface Note{
    id?: number;
    title: string;
    text: string;
    date: number;
    tags: string[];
    carnet: number;
}