export interface task{
    id?: number;
    test: string;
    done: boolean;
}

export interface Todo{
    id?: number;
    title: string;
    date: Date;
    list: [task];
    tags: [String];
    carnet: number;
    done: boolean;
}
