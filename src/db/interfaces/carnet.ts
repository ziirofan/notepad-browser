export interface Carnet {
    id?: number;
    title: string;
    numberOfNotes: number;
}